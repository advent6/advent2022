from helpers import get_lines_from_file

DAY = 7

directory_sizes = {}
cur = 0


def get_directories_size(lines: list[str]) -> int:
    global cur

    directory = f"{lines[cur].split()[-1]}{cur}"
    size = 0
    cur += 1

    while cur < len(lines):
        line = lines[cur]

        if line == "$ cd ..":
            directory_sizes[directory] = size
            return size

        elif line == "$ ls" or line.startswith("dir "):
            pass

        elif line[0].isdigit():
            size += int(line.split()[0])

        elif line.startswith("$ cd "):
            size += get_directories_size(lines)

        else:
            raise ValueError(f"Unsupported command: {line}")

        cur += 1

    else:
        directory_sizes[directory] = size
        return size


def run1() -> int:
    lines = get_lines_from_file(DAY)
    get_directories_size(lines)
    return sum([size for size in directory_sizes.values() if size <= 100000])


def run2() -> int:
    lines = get_lines_from_file(DAY)
    total_size = get_directories_size(lines)
    needed_space = 30000000 - (70000000 - total_size)
    return min([size for size in directory_sizes.values() if size > needed_space])
