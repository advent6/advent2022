from helpers import get_lines_from_file

DAY = 6


def find_different_characters(line: str, length: int) -> int:
    signal = line[:length]
    i = length
    while len(set(signal)) != length:
        i += 1
        signal = line[i - length:i]

    return i


def run1() -> int:
    line = get_lines_from_file(DAY)[0]
    return find_different_characters(line, 4)


def run2() -> int:
    line = get_lines_from_file(DAY)[0]
    return find_different_characters(line, 14)
