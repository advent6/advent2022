import re

from more_itertools import split_at

from helpers import get_lines_from_file

DAY = 5


def parse_stacks(raw_stacks: list[str]) -> list[list[str]]:
    horizontal_stacks = [line[1::4] for line in raw_stacks]
    stack_count = len(horizontal_stacks)
    height = len(horizontal_stacks[0])
    return [
        [
            crate
            for j in range(stack_count)
            if (crate := horizontal_stacks[stack_count - j - 1][i]) != " "
        ]
        for i in range(height)
    ]


def parse_moves(raw_moves: list[str]) -> list[tuple[int]]:
    pattern = re.compile(r"\d+")
    return [tuple(map(int, pattern.findall(line))) for line in raw_moves]


def parse_input() -> tuple[list[list[str]], list[tuple[int]]]:
    lines = get_lines_from_file(DAY, strip_whitespace=False)
    raw_stacks, raw_moves = split_at(lines, lambda i: i is None, maxsplit=2)
    stacks = parse_stacks(raw_stacks[:-1])
    moves = parse_moves(raw_moves)
    return stacks, moves


def run(take_multiple: bool) -> str:
    stacks, moves = parse_input()

    def move_crates(amount: int, source: int, destination: int):
        # Slicing would be faster, but if we wanted faster instead of prettier we wouldn't write in Python
        crates = [stacks[source - 1].pop() for _ in range(amount)]
        if take_multiple:
            crates.reverse()
        stacks[destination - 1].extend(crates)

    for move in moves:
        move_crates(*move)

    return "".join([stack[-1] for stack in stacks])


def run1() -> str:
    return run(False)


def run2() -> str:
    return run(True)
