from more_itertools import split_at

from helpers import get_lines_from_file

DAY = 1


def get_calories_by_elf():
    lines = get_lines_from_file(DAY, type_=int)
    return [sum(elf_calories) for elf_calories in split_at(lines, lambda l: l is None)]


def run1() -> int:
    calories_by_elf = get_calories_by_elf()
    return max(calories_by_elf)


def run2() -> int:
    calories_by_elf = get_calories_by_elf()
    return sum(sorted(calories_by_elf, reverse=True)[:3])
