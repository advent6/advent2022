from typing import Callable


def get_lines_from_file(day: int, file_name: str = "input.txt", type_: Callable = None, strip_whitespace=True) -> list:
    lines = []
    with open(f"day{day}/{file_name}") as f:
        for line in f.readlines():
            strip_args = [] if strip_whitespace else ["\n"]
            line = line.strip(*strip_args) or None
            if line is not None and type_ is not None:
                line = type_(line)
            lines.append(line)
    return lines
