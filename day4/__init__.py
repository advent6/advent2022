from helpers import get_lines_from_file

DAY = 4


def parse_elf_sections(assignment: str) -> set[int]:
    first_section, last_section = map(int, assignment.split("-"))
    return set(range(first_section, last_section + 1))


def get_pairs() -> list[tuple[set[int]]]:
    lines = get_lines_from_file(DAY)
    return [tuple(map(parse_elf_sections, line.split(","))) for line in lines]


def run1() -> int:
    pairs = get_pairs()
    # cnt = len([1 for r1, r2 in pairs if r1.issubset(r2) or r2.issubset(r1)])
    # afterthought to avoid double comparison and boost performance to the sky
    cnt = len([1 for r1, r2 in pairs if len(r1.intersection(r2)) == min(len(r1), len(r2))])
    return cnt


def run2() -> int:
    pairs = get_pairs()
    cnt = len([1 for r1, r2 in pairs if r1.intersection(r2)])
    return cnt
