from enum import Enum

from helpers import get_lines_from_file

DAY = 2


class Moves(int, Enum):
    ROCK = 1
    PAPER = 2
    SCISSORS = 3


class GameRound:
    def __init__(self, opponent_move, my_move=None, outcome=None):
        self.opponent_move = self.parse_move(opponent_move)
        if my_move:
            self.my_move = self.parse_move(my_move)
            self.outcome = self.calculate_outcome()
        else:
            self.outcome = (self.parse_move(outcome) - 1) * 3  # magic
            self.my_move = self.calculate_move()
        self.score = self.my_move + self.outcome

    @staticmethod
    def parse_move(move: str) -> Moves:
        match move:
            case ("A" | "X"):
                return Moves.ROCK
            case ("B" | "Y"):
                return Moves.PAPER
            case ("C" | "Z"):
                return Moves.SCISSORS

    def calculate_outcome(self) -> int:
        if self.opponent_move == self.my_move:
            return 3
        if self.opponent_move - self.my_move == 1:
            return 0
        if self.my_move - self.opponent_move == 1:
            return 6
        return 6 if self.my_move == Moves.ROCK else 0

    def calculate_move(self) -> Moves:
        if self.outcome == 3:
            return self.opponent_move
        if self.outcome == 0:
            return self.opponent_move - 1 or Moves.SCISSORS
        return self.opponent_move + 1 if self.opponent_move != Moves.SCISSORS else Moves.ROCK


def get_rounds(second_param_name: str):
    lines = get_lines_from_file(DAY)
    rounds = []
    for line in lines:
        opponent_move, second_param = line.split()
        rounds.append(GameRound(opponent_move, **{second_param_name: second_param}))
    return rounds


def run1() -> int:
    return sum([gr.score for gr in get_rounds("my_move")])


def run2() -> int:
    return sum([gr.score for gr in get_rounds("outcome")])
