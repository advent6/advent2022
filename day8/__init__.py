from helpers import get_lines_from_file

DAY = 8


def get_trees() -> list[list[int]]:
    lines = get_lines_from_file(DAY)
    return [list(map(int, line)) for line in lines]


def get_tallest_tree_index(row: list[int], reverse: bool = False) -> int:
    tallest_tree_height = max(row)
    if reverse:
        return next(len(row) - i - 1 for i, val in enumerate(row[::-1]) if val == tallest_tree_height)
    else:
        return row.index(tallest_tree_height)


def get_row_visible_trees(row: list[int]) -> list[int]:
    indices = []
    length = len(row)
    tallest_tree_index = get_tallest_tree_index(row)
    indices.append(tallest_tree_index)
    cur = tallest_tree_index

    while cur != 0:
        current_tallest_tree_index = get_tallest_tree_index(row[0:cur])
        indices.append(current_tallest_tree_index)
        cur = current_tallest_tree_index

    cur = tallest_tree_index
    while cur != length - 1:
        current_tallest_tree_index = get_tallest_tree_index(row[cur + 1:length], reverse=True)
        indices.append(current_tallest_tree_index + cur + 1)
        cur += (current_tallest_tree_index + 1)

    return indices


def get_visibility_map() -> list[list[int]]:
    trees = get_trees()
    length = len(trees)
    width = len(trees[0])

    visibility_map = [
        [
            True if i == 0 or i == length - 1 or j == 0 or j == width - 1 else False
            for j in range(width)
        ]
        for i in range(length)
    ]

    for i in range(1, length - 1):
        for visible_index in get_row_visible_trees(trees[i]):
            visibility_map[i][visible_index] = True

    for j in range(1, width - 1):
        for visible_index in get_row_visible_trees([trees[i][j] for i in range(length)]):
            visibility_map[visible_index][j] = True

    return visibility_map


def run1() -> int:
    visibility_map = get_visibility_map()
    return len([1 for row in visibility_map for is_visible in row if is_visible])


def get_visible_trees_count(height: int, row: list[int]) -> int:
    return next((i + 1 for i, value in enumerate(row) if value >= height), len(row))


def get_scenic_map() -> list[list[int]]:
    trees = get_trees()
    length = len(trees)
    width = len(trees[0])
    scenic_map = []

    for i in range(1, length - 1):
        row_scenic_map = []

        for j in range(1, width - 1):
            height = trees[i][j]
            right = get_visible_trees_count(height, trees[i][j + 1:])
            left = get_visible_trees_count(height, trees[i][j - 1::-1])
            up = get_visible_trees_count(height, [trees[ii][j] for ii in range(i - 1, -1, -1)])
            down = get_visible_trees_count(height, [trees[ii][j] for ii in range(i + 1, length)])
            row_scenic_map.append(right * left * up * down)

        scenic_map.append(row_scenic_map)

    return scenic_map


def run2() -> int:
    scenic_map = get_scenic_map()
    return max([score for row in scenic_map for score in row])
