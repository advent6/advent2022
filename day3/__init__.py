from more_itertools import chunked

from helpers import get_lines_from_file

DAY = 3


def get_priority(item: str) -> int:
    if item.islower():
        return ord(item) - 96
    return ord(item) - 38


def run1() -> int:
    lines = get_lines_from_file(3)
    res = 0
    for line in lines:
        middle = len(line) // 2
        comp1, comp2 = line[:middle], line[middle:]
        common_item = set(comp1).intersection(comp2).pop()
        res += get_priority(common_item)
    return res


def run2() -> int:
    lines = get_lines_from_file(3)
    res = 0
    for group in chunked(lines, 3, strict=True):
        common_item = set(group[0]).intersection(group[1]).intersection(group[2]).pop()
        res += get_priority(common_item)
    return res
